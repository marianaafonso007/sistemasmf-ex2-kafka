package com.sistemamf.validacao.service;

import com.sistemamf.validacao.client.ConsultarCnpjClient;
import com.sistemamf.validacao.client.EmpresaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoService {
    @Autowired
    private ConsultarCnpjClient consultarCnpjClient;

    public EmpresaDTO validacaoEmpresa(String cnpj){
        EmpresaDTO empresa = consultarCnpjClient.getById(cnpj);
        System.out.println(empresa.getNome() + " " + empresa.getCnpj() + " " + empresa.getCapital_social() + " Deu Certo !!!");
        return empresa;
    }
}
