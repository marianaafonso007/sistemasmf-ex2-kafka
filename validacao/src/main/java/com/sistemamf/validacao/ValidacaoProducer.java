package com.sistemamf.validacao;

import com.sistemamf.validacao.client.EmpresaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ValidacaoProducer {
    @Autowired
    private KafkaTemplate<String, EmpresaDTO> producer;

    public void enviarAoKafka(EmpresaDTO empresa) {
        producer.send("spec2-mariana-afonso-2", empresa);
    }
}
