package com.sistemamf.validacao.controller;

import com.sistemamf.empresa.model.Empresa;
import com.sistemamf.validacao.ValidacaoProducer;
import com.sistemamf.validacao.client.EmpresaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ValidacaoController {
    @Autowired
    private ValidacaoProducer validacaoProducer;

    @PostMapping
    public void cadastrarEmpresa(@RequestBody EmpresaDTO empresaDTO){
        validacaoProducer.enviarAoKafka(empresaDTO);
    }
}
