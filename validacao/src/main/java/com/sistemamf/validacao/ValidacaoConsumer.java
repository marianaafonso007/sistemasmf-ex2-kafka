package com.sistemamf.validacao;

import com.sistemamf.empresa.model.Empresa;
import com.sistemamf.validacao.client.EmpresaDTO;
import com.sistemamf.validacao.controller.ValidacaoController;
import com.sistemamf.validacao.service.ValidacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ValidacaoConsumer {
    @Autowired
    private ValidacaoService validacaoService;

    @Autowired
    private ValidacaoController validacaoController;

    @KafkaListener(topics = "spec2-mariana-afonso-0", groupId = "teste1")
    public void receber(@Payload Empresa empresa) {
        System.out.println("O CNPJ que chegou foi: " + empresa.getCnpj());
        EmpresaDTO empresaDTO =  validacaoService.validacaoEmpresa(empresa.getCnpj());
        validacaoController.cadastrarEmpresa(empresaDTO);
    }
}
