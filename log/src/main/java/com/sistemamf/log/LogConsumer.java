package com.sistemamf.log;

import com.sistemamf.log.service.LogService;
import com.sistemamf.validacao.client.EmpresaDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class LogConsumer {
    @Autowired
    private LogService logService;

    @KafkaListener(topics = "spec2-mariana-afonso-2", groupId = "teste")
    public void receber(@Payload EmpresaDTO empresa) {
        System.out.println("O CNPJ " + empresa.getCnpj());
        logService.gravarLog(empresa);
    }
}
