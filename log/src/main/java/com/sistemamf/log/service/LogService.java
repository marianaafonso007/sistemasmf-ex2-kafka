package com.sistemamf.log.service;

import com.sistemamf.empresa.model.Empresa;
import com.sistemamf.validacao.client.EmpresaDTO;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class LogService {
    public void gravarLog(EmpresaDTO empresa) {
        try {
            File arquivo = new File( "log.csv");
            StringBuilder sb = new StringBuilder();
            if (!arquivo.exists()) {
                arquivo.createNewFile();
                sb.append("CNPJ Empresa");
                sb.append(";");
                sb.append("Nome Empresa");
                sb.append(";");
                sb.append("Capital_social");
                sb.append(";");
                sb.append("Status Final");
                sb.append('\n');
            }
            sb.append(empresa.getCnpj());
            sb.append(";");
            sb.append(empresa.getNome());
            sb.append(";");
            sb.append(empresa.getCapital_social());
            sb.append(";");
            if(Double.parseDouble(empresa.getCapital_social()) < 1000.00){
                sb.append("Não Aceita");
            }else {
                sb.append("Aceita");
            }
            sb.append('\n');

            FileWriter fw = new FileWriter( arquivo, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(sb.toString());
            bw.close();fw.close();

            System.out.println("Log Salvo!");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
