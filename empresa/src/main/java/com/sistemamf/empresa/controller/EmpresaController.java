package com.sistemamf.empresa.controller;

import com.sistemamf.empresa.model.Empresa;
import com.sistemamf.empresa.producer.EmpresaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmpresaController {
    @Autowired
    private EmpresaProducer empresaProducer;

    @PostMapping
    public void cadastrarEmpresa(@RequestBody Empresa empresa){
        empresaProducer.enviarAoKafka(empresa);
    }
}
