package com.sistemamf.empresa.client;

import com.sistemamf.empresa.model.Empresa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cnpj")
public interface ConsultarCnpjClient {
    @GetMapping("/{cnpj}")
    Empresa getById(@PathVariable String cnpj);

}
