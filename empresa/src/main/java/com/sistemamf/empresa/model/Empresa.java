package com.sistemamf.empresa.model;

public class Empresa {
    private String cnpj;

    public Empresa() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
}
